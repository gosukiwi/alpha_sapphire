from lib.tokenizer.matchers.whitespace import Whitespace
import pytest

@pytest.fixture
def matcher():
    return Whitespace()

def test_matches(matcher):
    assert matcher.is_match(' ') == True
    assert matcher.is_match("\n") == True
    assert matcher.is_match("\t") == True
    assert matcher.is_match("\t  \n \t\n\n ") == True

def test_does_not_match(matcher):
    assert matcher.is_match('a') == False
    assert matcher.is_match('1') == False
    assert matcher.is_match('!') == False

def test_token(matcher):
    assert matcher.is_match("\t  \n \t\n\n ") == True
    assert matcher.token == None

def test_consume(matcher):
    matcher.is_match("\t  \n \t\n\n ")
    assert matcher.consume("\t  \n \t\n\n foo") == "foo"
