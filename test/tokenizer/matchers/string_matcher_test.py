from lib.tokenizer.matchers.string_matcher import StringMatcher
import pytest

@pytest.fixture
def matcher():
    return StringMatcher()

def test_matches_simple(matcher):
    assert matcher.is_match('""') == True
    assert matcher.token.value == ''

    assert matcher.is_match('"hello, world"') == True
    assert matcher.token.value == "hello, world"

    assert matcher.is_match('"hello, world') == False
    assert matcher.is_match('"') == False
    assert matcher.is_match('foo') == False

def test_matches_escaping(matcher):
    assert matcher.is_match('"hello, \\"world\\""') == True
    assert matcher.token.value == 'hello, "world"'

    assert matcher.is_match('"hello \\ \\"world\\""') == True
    assert matcher.token.value == 'hello \\ "world"'

def test_single_quotes(matcher):
    assert matcher.is_match('""') == True
    assert matcher.token.value == ''

    assert matcher.is_match("'hello, world'") == True
    assert matcher.token.value == "hello, world"

    assert matcher.is_match("'hello, \\'world\\''") == True
    assert matcher.token.value == "hello, 'world'"
