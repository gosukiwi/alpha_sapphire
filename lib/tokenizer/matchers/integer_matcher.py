from lib.tokenizer.tokens.token import Token
from lib.tokenizer.matchers.matcher import Matcher

class IntegerMatcher(Matcher):
    def is_match(self, text):
        self._len = 0
        index = 0
        while True:
            if index >= len(text):
                break
            char = ord(text[index])
            if char >= ord('0') and char <= ord('9'):
                index += 1
            else:
                break

        if index > 0:
            self._len = index
            self.token = Token('INTEGER', text[0:self._len])

        return self._len > 0
