from lib.interpreter.evaluators.evaluator import Evaluator
from lib.interpreter.objects.string import String

class StringEvaluator(Evaluator):
    def eval(self, node, context):
        return String(value=node.content)
