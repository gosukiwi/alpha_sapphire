class Token:
    def __init__(self, name, value, line=0, col=0):
        self.name  = name
        self.value = value
        self.line  = line
        self.col   = col

    def __str__(self):
        return "[%s `%s']" % (self.name, self.value)
