from lib.parser.parsers.expression_parser import ExpressionParser
from lib.parser.parsers.unop_parser import UnopParser
from lib.parser.parsers.binop_parser import BinopParser
from lib.parser.parsers.assign_parser import AssignParser
from lib.parser.parsers.atom_parser import AtomParser
from lib.parser.parsers.identifier_parser import IdentifierParser
from lib.parser.parsers.string_parser import StringParser
from lib.parser.parsers.integer_parser import IntegerParser
from lib.parser.parsers.fn_parser import FnParser
from lib.parser.parsers.plist_parser import PListParser
from lib.parser.parsers.expression_list_parser import ExpressionListParser
from lib.parser.parsers.arg_list_parser import ArgListParser
from lib.parser.parsers.arg_list_parser import PArgListParser
from lib.parser.parsers.call_parser import CallParser
from lib.parser.parsers.call_chain_parser import CallChainParser
from lib.parser.parsers.pexpr_parser import PExprParser
from lib.parser.parsers.pexpr_parser import PExprOrChainCallParser

class ParserFactory:
    parsers = {
        'expression': ExpressionParser,
        'unop': UnopParser,
        'binop': BinopParser,
        'assign': AssignParser,
        'atom': AtomParser,
        'identifier': IdentifierParser,
        'string': StringParser,
        'integer': IntegerParser,
        'fn': FnParser,
        'plist': PListParser,
        'expression_list': ExpressionListParser,
        'arg_list': ArgListParser,
        'parg_list': PArgListParser,
        'call': CallParser,
        'call_chain': CallChainParser,
        'paren_expr': PExprParser,
        'pexpr_or_chain': PExprOrChainCallParser,
    }

    @staticmethod
    def parser_for(rule):
        return ParserFactory.parsers[rule]()
