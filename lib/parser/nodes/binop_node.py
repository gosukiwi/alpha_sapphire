from lib.parser.nodes.node import Node

class BinopNode(Node):
    def __init__(self, lhs, op, rhs):
        Node.__init__(self, "BINOP", None)
        self.lhs = lhs
        self.op = op
        self.rhs = rhs

    def __str__(self):
        return "[%s %s %s]" % (self.op, self.lhs, self.rhs)
