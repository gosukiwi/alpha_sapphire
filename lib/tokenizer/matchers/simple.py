from lib.tokenizer.tokens.token import Token
from lib.tokenizer.matchers.matcher import Matcher

class Simple(Matcher):
    def __init__(self, name, str_to_match):
        Matcher.__init__(self)
        self._name = name
        self._str  = str_to_match
        self._len  = len(str_to_match)

    def is_match(self, input):
        if not input.startswith(self._str):
            return False

        self.token = Token(self._name, self._str)
        return True

    def consume(self, input):
        return input[self._len:]
