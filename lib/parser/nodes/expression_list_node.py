from lib.parser.nodes.node import Node

class ExpressionListNode(Node):
    def __init__(self, nodes):
        Node.__init__(self, "EXPRESSION_LIST", None)
        self.nodes = nodes

    def __str__(self):
        return "EXPRESSION LIST"
