from lib.tokenizer.tokens.token import Token
import pytest

@pytest.fixture
def token():
    return Token("some-name", "some-value")

def test_name(token):
    assert token.name == "some-name"

def test_value(token):
    assert token.value == "some-value"

def test_as_str(token):
    assert token.__str__() == "[some-name `some-value']"
