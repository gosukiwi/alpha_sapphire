from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.fn_node import FnNode

# function_definition := FN <param_list> ARROW <expression_list>
class FnParser(BaseParser):
    def is_match(self, tokens):
        if not self._peek(tokens, "FN"):
            return False

        plist_parser = self._parser_for('plist')
        if not plist_parser.is_match(tokens[1:]):
            return False
        consumed = 1 + plist_parser.consumed

        if not self._peek(tokens[consumed:], "ARROW"):
            return False
        consumed += 1

        expression_list_parser = self._parser_for('expression_list')
        if not expression_list_parser.is_match(tokens[consumed:]):
            return False

        self.node = FnNode(args=plist_parser.node, body=expression_list_parser.node)
        self.consumed = consumed + expression_list_parser.consumed
        return True
