from lib.parser.parser import Parser
from lib.tokenizer.tokenizer import Tokenizer
from lib.interpreter.interpreter import Interpreter
from lib.interpreter.objects.symbol import Symbol

def parse(text):
    tokenizer = Tokenizer()
    parser = Parser()
    tokens = tokenizer.tokenize(text)
    return parser.parse(tokens)

def to_sym(text):
    return Symbol(text)

def test_interpreter():
    interpreter = Interpreter()
    interpreter.eval(parse("a = 2"))
    a = to_sym('a')
    assert interpreter.world[a].value == 2
    assert interpreter.world[a].type == 'Integer'

    interpreter.eval(parse("b = 'hi'"))
    b = to_sym('b')
    assert interpreter.world[b].value == 'hi'
    assert interpreter.world[b].type == 'String'
