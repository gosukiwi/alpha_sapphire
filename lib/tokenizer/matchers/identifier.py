from lib.tokenizer.tokens.token import Token
from lib.tokenizer.matchers.matcher import Matcher

# An identifier is a sequence of letters and strings, starting with a letter.
# [a-zA-Z_][a-zA-Z0-9_-?]*
# Some valid identifirs: foo, _foo, foo1, bar_foo, Foo, FOO, is_foo?
#
class Identifier(Matcher):
    def is_match(self, text):
        self._len = 0
        if self._is_valid_initial_char(text[0]):
            self._len = 1
            limit = len(text)
            while self._len < limit and self._is_valid_char(text[self._len]):
                self._len += 1
            self.token = Token('IDENTIFIER', text[0:self._len])

        return self._len > 0

    def _is_valid_initial_char(self, ch):
        ordinal = ord(ch)
        return (ordinal >= ord('a') and ordinal <= ord('z')) or (ordinal >= ord('A') and ordinal <= ord('Z')) or ch == '_'

    def _is_valid_char(self, ch):
        ordinal = ord(ch)
        return self._is_valid_initial_char(ch) or (ordinal >= ord('0') and ordinal <= ord('9')) or ch == '-' or ch == '?'
