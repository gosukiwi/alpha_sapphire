from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.arg_list_node import ArgListNode

# arg_list := <atom> COMMA <arg_list>
#           | <atom>
#           | Nothing
#
# parg_list := POPEN <arg_list> PCLOSE
#
class ArgListParser(BaseParser):
    def is_match(self, tokens):
        atom_parser = self._parser_for('atom')
        if not atom_parser.is_match(tokens):
            self.node = ArgListNode([])
            return True
        consumed = atom_parser.consumed

        atom_node = atom_parser.node
        if not self._peek(tokens[consumed:], 'COMMA'):
            self.consumed = consumed
            self.node = ArgListNode([atom_node])
            return True
        consumed += 1

        arg_list_parser = ArgListParser()
        if not arg_list_parser.is_match(tokens[consumed:]) or len(arg_list_parser.node.nodes) is 0:
            return False

        self.node = ArgListNode([atom_node] + arg_list_parser.node.nodes)
        self.consumed = consumed + arg_list_parser.consumed
        return True

class PArgListParser(BaseParser):
    def is_match(self, tokens):
        self.node = ArgListNode([])

        if not self._peek(tokens, 'POPEN'):
            return False

        if self._peek(tokens, 'PCLOSE'):
            self.consumed = 2
            return True

        arg_list_parser = self._parser_for('arg_list')
        if not arg_list_parser.is_match(tokens[1:]):
            return False
        consumed = 1 + arg_list_parser.consumed

        if not self._peek(tokens[consumed:], 'PCLOSE'):
            return False
        consumed += 1

        self.consumed = consumed
        self.node = arg_list_parser.node
        return True
