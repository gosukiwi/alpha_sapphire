import lib.parser.parsers.parser_factory
from lib.parser.nodes.node import Node

# Base parser. All parsers implement `is_match`. It's VERY IMPORTANT to use
# the received tokens as read-only.
#
# Parsers are supposed to be used once. If they match, they hold a `node` as
# well as a `consumed` property. The first is an instance of a Node representing
# this rule for use in the AST. The latter is the amount of tokens consumed.
#
class BaseParser:
    def __init__(self):
        self.consumed = 0
        self.node = Node.none()

    def _peek(self, tokens, name):
        if len(tokens) == 0:
            return False
        return tokens[0].name == name

    def _try_parser(self, name, tokens):
        parser = self._parser_for(name)
        if parser.is_match(tokens):
            self.node = parser.node
            self.consumed = parser.consumed
            return True
        return False

    def _try_parsers(self, names, tokens):
        for name in names:
            if self._try_parser(name, tokens):
                return True
        return False

    def _parser_for(self, name):
        return lib.parser.parsers.parser_factory.ParserFactory.parser_for(name)
