from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.string_node import StringNode

class StringParser(BaseParser):
    def is_match(self, tokens):
        if not self._peek(tokens, "STRING"):
            return False

        self.node = StringNode(tokens[0].value)
        self.consumed = 1
        return True
