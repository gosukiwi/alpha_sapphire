from lib.interpreter.evaluators.factory import Factory

class Interpreter:
    def __init__(self, debug=True):
        self.world = {} # a dictionary of Symbol => Object. Could be replaced by an object if needed.
        self.debug = debug

    def eval(self, statements):
        for node in statements:
            result = self.eval_for(node.name).eval(node, self.world)
            if self.debug:
                print "[DEBUG] Compiled:", node
                if result != None:
                    print "[DEBUG] Result:", result
                print "[DEBUG] World:", self.world

    def eval_for(self, name):
        return Factory.eval_for(name)
