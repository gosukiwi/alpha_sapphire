from lib.parser.nodes.node import Node

class AssignNode(Node):
    def __init__(self, target, value):
        Node.__init__(self, "ASSIGN", None)
        self.value = value
        self.target = target

    def __str__(self):
        return "Assign %s to `%s'" % (self.value, self.target)
