from lib.parser.nodes.node import Node

class CallNode(Node):
    def __init__(self, message, args, chain=Node.none()):
        Node.__init__(self, "CALL", None)
        self.message = message
        self.args = args
        self.chain = chain

    def __str__(self):
        return "Call"
