from lib.parser.nodes.node import Node

class ArgListNode(Node):
    def __init__(self, nodes):
        Node.__init__(self, "ARG_LIST", None)
        self.nodes = nodes

    def __str__(self):
        return "ARG_LIST"
