from lib.tokenizer.matchers.identifier import Identifier
import pytest

@pytest.fixture
def matcher():
    return Identifier()

def test_matches_simple(matcher):
    assert matcher.is_match('my_var') == True
    assert matcher.is_match('1a') == False
    assert matcher.is_match('-asd') == False

def test_matches_complex(matcher):
    assert matcher.is_match('mY_V-ar123?') == True
