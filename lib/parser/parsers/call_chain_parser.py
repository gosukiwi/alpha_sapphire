from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.call_node import CallNode

# call_chain := <call> DOT <call_chain>
#             | <call>
class CallChainParser(BaseParser):
    def is_match(self, tokens):
        call_parser = self._parser_for('call')
        if not call_parser.is_match(tokens):
            return False
        consumed = call_parser.consumed

        if not self._peek(tokens[consumed:], 'DOT'):
            self.node = call_parser.node
            self.consumed = consumed
            return True
        consumed += 1

        call_chain_parser = CallChainParser()
        if not call_chain_parser.is_match(tokens[consumed:]):
            return False
        consumed += call_chain_parser.consumed

        node = call_parser.node
        node.chain = call_chain_parser.node
        self.node = node
        self.consumed = consumed
        return True
