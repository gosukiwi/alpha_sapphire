from lib.parser.parsers.parser_factory import ParserFactory
from lib.tokenizer.tokenizer import Tokenizer

def tokenize(text):
    return Tokenizer().tokenize(text)

def test_one():
    parser = ParserFactory.parser_for('fn')
    assert parser.is_match(tokenize("""
    fn foo ->
        a = 1
        a + b
    end
    """)) == True
    assert parser.node.name == 'FN'
    assert parser.node.body.name == 'EXPRESSION_LIST'
    assert parser.node.args.name == 'PLIST'


def test_no_args():
    parser = ParserFactory.parser_for('fn')
    assert parser.is_match(tokenize("""
    fn ->
        a = 1
        a + b
    end
    """)) == True
    assert parser.node.name == 'FN'
    assert parser.node.body.name == 'EXPRESSION_LIST'
    assert parser.node.args.name == 'PLIST'

def test_many_args():
    parser = ParserFactory.parser_for('fn')
    assert parser.is_match(tokenize("""
    fn foo, bar, baz ->
        a = 1
        a + b
    end
    """)) == True
    assert parser.node.args.name == 'PLIST'
    assert len(parser.node.args.nodes) == 3

def test_invalid():
    parser = ParserFactory.parser_for('fn')
    assert parser.is_match(tokenize("""
    fn
        a = 1
        a + b
    end
    """)) == False

    assert parser.is_match(tokenize("""
    fn ->
        a = 1
        a + b
    """)) == False
