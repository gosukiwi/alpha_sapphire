class Node:
    def __init__(self, name, value):
        self.name = name
        self.value = value

    def __str__(self):
        return "[%s %s]" % (self.name, self.value)

    @staticmethod
    def none():
        return Node(name="NULL",value=None)

    def is_none(self):
        return self.name is "NULL" and self.value is None
