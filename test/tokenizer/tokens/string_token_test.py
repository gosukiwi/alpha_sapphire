from lib.tokenizer.tokens.string_token import StringToken
import pytest

@pytest.fixture
def token():
    return StringToken("some-value", "SOMETYPE")

def test_name(token):
    assert token.name == "STRING"

def test_value(token):
    assert token.value == "some-value"

def test_type(token):
    assert token.type == "SOMETYPE"

def test_as_str(token):
    assert token.__str__() == "[STRING `some-value' SOMETYPE]"
