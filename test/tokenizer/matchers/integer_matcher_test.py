from lib.tokenizer.matchers.integer_matcher import IntegerMatcher
import pytest

@pytest.fixture
def matcher():
    return IntegerMatcher()

def test_matches_integers(matcher):
    assert matcher.is_match('1') == True
    assert matcher.is_match('001') == True
    assert matcher.is_match('918239127') == True
    assert matcher.is_match('zzzzz') == False
