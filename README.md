# alpha_sapphire ![as](https://codeship.com/projects/427534c0-2169-0134-6e56-1eef50e7a565/status?branch=master)
Simple, object oriented, dynamic, functional programming language for humans.

# Contributing
Alpha Sapphire is written in RPython, a less dynamic subset of Python which can
be compiled to efficient machine code. Because of this, you can work on
alpha_sapphire using Python 2.7 (either CPython or PyPy) for a faster
development loop, as all RPython code is valid Python code (but not the other
way around).

You are encouraged to use TDD but as long as there are tests for the feature you
are working on, the order doesn't matter.

Because Python is used for tests, we must make sure the code is valid RPython.
The easiest way to do this is building the project. If the build passes then we
are sure it's valid RPython, if not, we'll get a _hopefully_ friendly error
message which we can use to fix our code.

So, summing it up:

  * Develop using Python 2.7: `python run.py`
  * Run tests with `py.test`
  * Build with `bin/build`

## Running tests
First of all, install dependencies: `pip install requirements.txt`. Then just do
`py.test`.

Optionally, if you want pretty output, you can install `pip install
pytest-sugar`.

## Building
First of all, this only works for unix-like systems, read the next section if
you want to build for Windows.

In order to build alpha_sapphire, You'll need __Python 2.7__ and pypy's
repository, which includes the RPython binary. It's up to you how to install
Python, as for the repo you can easily clone it by running `hg clone
ssh://hg@bitbucket.org/pypy/pypy`. I use [Vagrant](https://www.vagrantup.com/)
to work on this project, so by default, the build script will assume the rpython
binary lives in `/home/vagrant/pypy/rpython/bin/rpython`.

If you want to move it somewhere else, just make an RPYTHON environmental
variable: `export RPYTHON=/some/full/path/for/pypy/rpython/bin/rpython`.

Now you are all set, just run `bin/build` to create an executable in
`bin/alpha_sapphire`. You can then move it and add it to your PATH just like any
other executable.

## Building on Windows
This is more complicated. For now the recommended way is to [use
bash](http://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/)
and just use the Linux executable, it has no overhead whatsoever, so for
development purposes should do just fine.

If you really want to make a Windows native executable, you'll have to follow
[RPython's documentation](http://doc.pypy.org/en/latest/windows.html).

# Continuous Integration
We use [Codeship for CI](https://codeship.com/projects/160944/), which also
[builds an ubuntu-compatible
binary](http://alpha-sapphire-builds.s3-website-us-west-2.amazonaws.com/).

At the moment, there's no automated Windows build. If you are interested in
setting up any CI service to make Windows executables then you are more than
welcome to submit a PR :)
