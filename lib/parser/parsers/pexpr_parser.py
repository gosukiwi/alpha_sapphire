from lib.parser.parsers.base_parser import BaseParser

# pexpr_or_call_chain := (<pexp> | <call_chain>)
# pexpr  := "(" <expression> ")"
#
class PExprOrChainCallParser(BaseParser):
    def is_match(self, tokens):
        return self._try_parsers([
            'paren_expr',
            'call_chain'
        ], tokens)

class PExprParser(BaseParser):
    def is_match(self, tokens):
        if not self._peek(tokens, "POPEN"):
            return False

        parser = self._parser_for('expression')
        if not parser.is_match(tokens[1:]):
            return False
        consumed = 1 + parser.consumed

        if not self._peek(tokens[consumed:], "PCLOSE"):
            return False

        self.node = parser.node
        self.consumed = consumed + 1
        return True
