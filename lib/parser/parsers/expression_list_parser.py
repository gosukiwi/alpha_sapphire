from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.expression_list_node import ExpressionListNode

# <expression_list> = <expression> <expression_list> END
#                   | <expression> END
#                   | END
class ExpressionListParser(BaseParser):
    def is_match(self, tokens):
        nodes = []
        consumed = 0
        while consumed < len(tokens):
            expression_parser = self._parser_for('expression')
            if not expression_parser.is_match(tokens[consumed:]):
                break
            nodes.append(expression_parser.node)
            consumed += expression_parser.consumed

        if self._peek(tokens[consumed:], 'END'):
            self.node = ExpressionListNode(nodes)
            self.consumed = consumed + 1
            return True

        return False
