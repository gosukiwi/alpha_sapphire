from lib.interpreter.evaluators.evaluator import Evaluator

class CallEvaluator(Evaluator):
    # This visitor will resolve a "call" node. The thing about alpha_sapphire is
    # that it's quite dynamic, so:
    #
    # foo = { "HI!" }
    # bar = "Bye"
    #
    # 1   => There is no context['1'] so this just evaluates to IntegerEvaluator(node), which returns an instance of Integer
    # foo => This should resolve to the result of invoking that function with no arguments
    # bar => This should resolve to context['bar'], which is "Bye"
    #
    # TODO: Chaining
    # 1.to_string => This is
    #
    # Are all
    def eval(self, node, context):
        # Evaluate the message. Identifiers evaluate to symbols.
        message = self.eval_one(node.message, context) # message can be an identifier, string or number (an atom)
        args = self.eval_many(node.args.nodes, context)

        if not node.chain.is_none():
            raise Exception("IMPLEMENT CHAIN CALL")

        if len(args) > 0: # Even though it's not a chain call, If I have args, it's a call!
            return context[message].send(message, args)

        # Not a chain call, not arguments. Check in context:
        if message in context:
            obj = context[message] # All AS' objects respond to 'type'
            if obj.type == 'Function':
                return obj.send(message, args)
            return obj

        # Ok it wasn't in the context either, just return it's eval'd value.
        return message
