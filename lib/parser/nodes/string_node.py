from lib.parser.nodes.node import Node

class StringNode(Node):
    def __init__(self, content):
        Node.__init__(self, "STRING", None)
        self.content = content

    def __str__(self):
        return self.content
