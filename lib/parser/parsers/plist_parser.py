from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.plist_node import PListNode

# param_list := <identifier> <comma> <param_list>
#             | <identifier>
#             | NOTHING
class PListParser(BaseParser):
    def is_match(self, tokens):
        identifier_parser = self._parser_for('identifier')
        if not identifier_parser.is_match(tokens):
            self.node = PListNode([])
            return True

        identifier_node = identifier_parser.node
        if not self._peek(tokens[1:], 'COMMA'):
            self.consumed = 1
            self.node = PListNode([identifier_node])
            return True

        plist_parser = PListParser()
        if not plist_parser.is_match(tokens[2:]) or len(plist_parser.node.nodes) is 0:
            return False

        self.node = PListNode([identifier_node] + plist_parser.node.nodes)
        self.consumed = 2 + plist_parser.consumed
        return True
