# alpha_sapphire
Alpha Sapphire, or alpha_sapphire, is a simple, interpreted, dynamic,
object-oriented language inspired by Ruby and Coffeescript. This document
explains the whole syntax of the language.

# MAYBEDO

 * Integrated warnings on some [CodeSmells](https://github.com/troessner/reek/blob/master/docs/Code-Smells.md).

# Expressive
The main goal of the language is to maximize expressiveness while keeping
the core of the language simple. This means, you can do things like:

    # Same, all set a to "true"
    a = true
    a = yes
    a = on
    # Same, all call "foo()" unless a is greater than 1
    foo() if a <= 1
    foo unless a > 1 # parens are optional if no arguments
    unless a > 1
      foo
    end

It's much easier to read something like:

    can_share = yes unless post.work_in_progress?

Than doing:

    if not post.work_in_progress?
      can_share = true
    end

## On Expresiveness
Syntax sugar is nice but it's not enough. Alpha Sappire's standard library is
designed to help developers write clean, object oriented code a-la Ruby and
Smalltalk.

    1.positive?
    2.negative?
    33.odd?
    12.even?
    0.zero?

# Functional
Functions -- or code blocks, are first-class citizens, so you can use them when
needed as you wish, a-la Ruby. A simple function looks like this:

    foo = fn name ->
        return "Hello, {0}!".format(name)
    end
    foo("Mike") # "Hello Mike!"

If the function is small, there's an alternative syntax which is quite good for
inling:

    foo = { name | "Hello, {0}!".format(name) }
    foo("Mike") # "Hello Mike!"

Of course, parameters are optional:

    bar = { 2 + 2 }
    bar() # 4

As you might have noted, the `return` is also optional, the last evaluated
expression is always returned.

You can receive functions as arguments and return them just like any other
value.

    addOne = { x | x + 1 }
    result = [1, 2, 3].map(addOne) # result is now [2, 3, 4]
    result = [1, 2, 3].map({ x | x + 1}) # this also works

    # this function returns another function which adds `n`
    # to it's argument
    addN = fn n ->
        fn x ->
          x + n
        end
    end
    # addOne could be implemented as such
    addOne = addN(1)

There is a special variable inside functions called `it`, which always points to
the first argument.

    foo = { it + 1 }
    foo(1) # 2

It's quite nice for inlining:

    [1, 2, 3, 4].map({ it + 1 }) # [2, 3, 4, 5]

When calling functions, arguments must be comma separated.

        foo(1, 2, 3)

If the function requires no arguments, the parenthesis are not needed.

    foo # call function foo

# Object Oriented
In alpha_sapphire, everything is an object. To keep things simple, object
orientation in the language is based on classes.

    class Foo
      foo_method ->
        "Hi!"
      end
    end

That returns a Class object, which could have been built like this:

    klass = Class.new("Foo")
    klass.add_instance_method("foo_method", { "Hi!" })
    instance = klass.new
    instance.foo_method() # "Hi!"

Object oriented programming in alpha_sapphire aims to be as simple as possible.
Thus, there is no inheritance or private methods, only mixins.

    class Foo
        greet ->
            "Hello, World!"
        end
    end

    class Bar < Foo
    end

    Bar.new.greet

Attributes are defined using `@`. Because in alpha_sapphire, every message the
objects receive calls a method, in order to access attributes from outside the
class you need to implement getters and setters.

    class Person
        init name ->
            @name = name
        end

        name ->
          @name
        end

        name= name ->
          @name = name
        end
    end

    p = Person.new("Mike")
    p.name          # "Mike"
    p.name = "John" # Sugar for to p.name=("John")
    p.name          # "John"

You are encouraged to use the helper methods `get`, `set` and `get_set`

    class Person
        get_set [:name] # generate `name` and `name=` methods
        get     [:age]  # only generate `age` method

        init name ->
            @name = name
        end
    end

How does that work? Besides method definitions, a class body can contain
instance function calls. So this could be implemented as such (note that this
version assumes `get_set` takes a single string argument instead of an array):

    # This class has a "get_set" static/shared method.
    class BaseObject
      get_set name ->
        add_instance_method(name      , { self.ivar(name) })
        add_instance_method("#{name}=", { val | self.ivar(name, val) })
      end
    end

    class Foo < BaseObject
      # Call "get_set" method, which uses reflection to add instance methods.
      get_set :bar

      init ->
        @bar = 1
      end
    end

    Foo.new.bar # 1

## Instances
Creating new instances is still a WIP, nevertheless, the outline aims to be
something like this:

  1. A new instance of `Object`, the base object, is created.
  2. For each object included, it adds them to the instance's `prototype` attribute.
  3. It calls the init method of the Class using the current instance as context.
  4. It evals all method calls using the current instance of the class as context.
  5. Returns the newly created instance.

## Prototype
All objects have a `prototype` attribute, which is just an array of objects.
When an object receives a message, alpha_sapphire tries to find a method with
the same name in that object. If it can't find it, it goes though all the
objects in the prototype, looking for an appropriate method recursively.

This means that the order in which you include other objects is important. As
the first method found will be called.

## Shared/Static methods and attributes
Methods and attributes can shared (static) across all instances:

    class Foo
        shared initialize ->
          @@bar = 18
        end

        shared some_method ->
          "I am #{@@bar} years old"
        end
    end

    Foo.initialize
    Foo.some_method # "I am 22 years old"

You can access shared stuff in instances too

    class Foo
        some_instance_method ->
            @@bar # 2
        end
    end

## Monkey Patching
Because alpha_sapphire is highly dynamic, you can override all objects, even
the "Standard Objects".

    String.add_instance_method("exclamate", { "#{self}!" })
    "hi".exclamate() # "hi!"

Another way would be

    String.reopen(fn self ->
      self.exclamate = { "#{self}!" }

      self.another_method = fn ->
        ...
      end
    end)

For static methods:

    String.reopen_class(fn klass ->
      klass.foo = { "FOO!" }
    end)
    String.foo() # FOO!

# Standard Objects
Objects which can be used in all programs.

## String
A lot of syntactic sugar is used to make string handling easier. Under the hood,
this object is in charge. The following statements are equal:

    a = "foo"
    a = String.new('foo')

Of course, using the `String` class is discouraged. You can do interpolation as
such:

    name = "Mike"
    print("Hello #{name}")
    print('Hello {0}'.format(name))
    print(String.new('Hello {0}').format(name))

As you can see, with double-quoted strings we can just use `#{}` to interpolate.
Single quotes assume there is no interpolation going on, so it's a bit faster.
If you want though you can use `String#format` to manually interpolate a-la C#.

This will fail:

    name = "Mi" + "ke"

The binary operation `+` works with all objects which
`responds_to?('binop_add')`. All numbers implement it, but not string, because
interpolation can be archieved in cleaner ways. If you really want you can
monkey-patch `String`, but it's highly discouraged.

# Binary Operations

 * `a > b`: Comparison, greater than.
 * `a >= b`: Comparison, greater or equal than.
 * `a < b`: Comparison, smaller than.
 * `a <= b`: Comparison, smaller or qeual than.
 * `a is b`: Comparison, equal.
 * `a isnt b`: Comparison, not equal.
 * `a or b`: Logical, or.
 * `a and b`: Logical, and.
 * `a + b`: Aritmetic, addition.
 * `a - b`: Aritmetic, substraction.
 * `a * b`: Aritmetic, multiplication.
 * `a / b`: Aritmetic, division.
 * `a % b`: Aritmetic, modulo.
 * `a ^ b`: Aritmetic, exponentiation.

# Unary Operations

 * `not a`

# Syntax at a glance

    a = 2
    name = "mike"
    let pi = 3.14 # cannot be changed

    some_fun = fn ->
      2 # implicit return
    end

    some_fun # call, parens when no args are optional

    class MyClass
      person = Person.new

      greet = fn name ->
        "Hi #{name}!"
      end

      # method call
      delegate :name, :person
    end

    # Mixins, object composition, method delegation, etc
    # If it can't find a method on Foo, it will search in Bar, then in Baz.
    # So if there is a repeated method, it uses the one which was included first
    class Foo
      include [Bar, Baz]
    end

    # Namespaces, just split them
    class Foo::Bar
    end
    Foo::Bar.new

    # Arrays
    arr = [1, 2, 3]
    arr.each({ print it }) # when no parameter is specified, default variable "it"
                           # points to the first parameter received

    # Hashes
    h = [ 'key': 1, foo: 'test str', 22: 'example' ]

    # Conditionals
    if true # or unless false
      something
    else
      something_else
    end

    unless false
      do_something()
    end

    foo() if true      # single line
    bar() unless false # same

    # Iteration, should be avoided, use each instead
    # or 10.times({ print (it + 1) })
    while true
      break
    end
