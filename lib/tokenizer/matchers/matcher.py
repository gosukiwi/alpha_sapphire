class Matcher:
    def __init__(self):
        self.token = None
        self._len = 0

    def consume(self, text):
        return text[self._len:]
