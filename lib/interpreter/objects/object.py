class Object:
    def __init__(self):
        self.prototype = []
        self.messages = {
            # The whole API of the object must be defined here.
            # 'send': SomeNativeMethodInstance()
            # 'add_message': SomeNativeMethodInstance()
            # 'respond_to?': SomeNativeMethodInstance()
            # 'new': NewFunctionNative()
        }
        self.type = 'Object'

    def send(self, message, args):
        if message in self.messages:
            return self.messages[message].send(args) # Invoke method, call function, send message, etc

        # Check inheritance
        for parent in self.prototype:
            if message in parent.messages:
                return parent.messages[message].send(args)

        # Native functions are just subclasses of Functions with hard-coded
        # functionality. They also responde to send(args)
        # TODO: Better exception name
        raise Exception("Invalid message")

    def add_message(self, name, fn):
        self.messages[name] = fn

    def to_string(self):
        print "IMPLEMENT ME"

    def __str__(self):
        return self.to_string()

    # __eq__ and __hash__ make sure we implement `hashable`
    def __eq__(self, other):
        return self.value == other.value

    def __hash__(self):
        return hash((self.type, self.value))
