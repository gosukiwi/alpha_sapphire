from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.assign_node import AssignNode

# assign := <identifier> EQUALS <expression> 
#
class AssignParser(BaseParser):
    def is_match(self, tokens):
        identifier_parser = self._parser_for('identifier')
        if not identifier_parser.is_match(tokens):
            return False

        if not self._peek(tokens[1:], "EQUALS"):
            return False

        consumed = 2 # We know we consumed only 2 tokens, IDENTIFIER and EQUALS.
        expression_parser = self._parser_for('expression')
        if not expression_parser.is_match(tokens[consumed:]):
            return False
        self.node = AssignNode(target=identifier_parser.node, value=expression_parser.node)
        self.consumed = consumed + expression_parser.consumed
        return True
