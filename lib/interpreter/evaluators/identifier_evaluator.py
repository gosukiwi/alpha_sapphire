from lib.interpreter.evaluators.evaluator import Evaluator
from lib.interpreter.objects.symbol import Symbol

class IdentifierEvaluator(Evaluator):
    def eval(self, node, context):
        return Symbol(value=node.identifier)
