from lib.tokenizer.tokenizer import Tokenizer
from lib.exceptions import SyntaxErrorException
import pytest

def tokenize(text):
    tokenizer = Tokenizer()
    return tokenizer.tokenize(text)

def test_tokenizer():
    result = tokenize('fn -> "hi" end')
    assert result[0].name == "FN"
    assert result[1].name == "ARROW"
    assert result[2].name == "STRING"
    assert result[3].name == "END"

def test_invalid():
    with pytest.raises(SyntaxErrorException):
        tokenize('.%1')
