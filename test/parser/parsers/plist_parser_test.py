from lib.parser.parsers.parser_factory import ParserFactory
from lib.tokenizer.tokenizer import Tokenizer

def tokenize(text):
    return Tokenizer().tokenize(text)

def test_one():
    parser = ParserFactory.parser_for('plist')
    assert parser.is_match(tokenize("foo")) == True
    assert len(parser.node.nodes) == 1

def test_none():
    parser = ParserFactory.parser_for('plist')
    assert parser.is_match(tokenize("")) == True
    assert len(parser.node.nodes) == 0

def test_many():
    parser = ParserFactory.parser_for('plist')
    assert parser.is_match(tokenize("foo, bar, baz")) == True
    assert parser.node.nodes[0].name == 'IDENTIFIER'
    assert parser.node.nodes[0].identifier == 'foo'
    assert parser.node.nodes[1].name == 'IDENTIFIER'
    assert parser.node.nodes[1].identifier == 'bar'
    assert parser.node.nodes[2].name == 'IDENTIFIER'
    assert parser.node.nodes[2].identifier == 'baz'
    assert len(parser.node.nodes) == 3
    assert parser.consumed == 5

def test_misisng():
    parser = ParserFactory.parser_for('plist')
    assert parser.is_match(tokenize("foo, bar,")) == False
    assert parser.node.is_none()
