from lib.parser.parsers.parser_factory import ParserFactory
from lib.tokenizer.tokenizer import Tokenizer

def tokenize(text):
    return Tokenizer().tokenize(text)

def test_call():
    parser = ParserFactory.parser_for('call_chain')
    assert parser.is_match(tokenize("foo()")) == True
    assert parser.node.name == 'CALL'

    assert parser.is_match(tokenize("foo('bar', 'baz', 2)")) == True
    assert parser.node.name == 'CALL'
    assert parser.node.args.name == 'ARG_LIST'
    assert len(parser.node.args.nodes) == 3

    assert parser.is_match(tokenize("foo")) == True
    assert parser.node.args.name == 'ARG_LIST'

    assert parser.is_match(tokenize("foo 1, 2")) == True
    assert parser.consumed == 1 # just consumed the first part

    assert parser.is_match(tokenize("'hi'")) == True

    assert parser.is_match(tokenize("foo.bar.baz")) == True
    assert parser.node.name == 'CALL'
    assert parser.node.chain.name == 'CALL'
    assert parser.node.chain.chain.name == 'CALL'
    assert parser.node.chain.message.identifier == 'bar'
    assert parser.node.chain.chain.message.identifier == 'baz'
