from lib.parser.parsers.parser_factory import ParserFactory
from lib.tokenizer.tokenizer import Tokenizer

def tokenize(text):
    return Tokenizer().tokenize(text)

def test_arg_list():
    parser = ParserFactory.parser_for('arg_list')
    assert parser.is_match(tokenize("1, 'foo', bar")) == True
    assert len(parser.node.nodes) == 3
    assert parser.node.nodes[0].name == 'INTEGER'
    assert parser.node.nodes[0].integer == 1
    assert parser.node.nodes[1].name == 'STRING'
    assert parser.node.nodes[2].name == 'IDENTIFIER'

def test_parg_list():
    parser = ParserFactory.parser_for('parg_list')
    assert parser.is_match(tokenize("(1, 'foo', bar)")) == True
    assert len(parser.node.nodes) == 3
    assert parser.node.nodes[0].name == 'INTEGER'
    assert parser.node.nodes[0].integer == 1
    assert parser.node.nodes[1].name == 'STRING'
    assert parser.node.nodes[2].name == 'IDENTIFIER'

def test_empty_arg_list():
    parser = ParserFactory.parser_for('arg_list')
    assert parser.is_match(tokenize("")) == True

def test_empty_parg_list():
    parser = ParserFactory.parser_for('parg_list')
    assert parser.is_match(tokenize("()")) == True
    assert parser.is_match(tokenize("")) == False
    assert parser.is_match(tokenize("(")) == False
