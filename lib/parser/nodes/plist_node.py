from lib.parser.nodes.node import Node

class PListNode(Node):
    def __init__(self, nodes):
        Node.__init__(self, "PLIST", None)
        self.nodes = nodes

    def __str__(self):
        return "PLIST"
