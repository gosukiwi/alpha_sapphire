from lib.parser.nodes.node import Node

class IntegerNode(Node):
    def __init__(self, integer):
        Node.__init__(self, "INTEGER", None)
        self.integer = integer

    def __str__(self):
        return str(self.integer)
