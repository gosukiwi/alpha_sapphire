from lib.parser.nodes.node import Node

class FnNode(Node):
    def __init__(self, args, body):
        Node.__init__(self, "FN", None)
        self.args = args
        self.body = body

    def __str__(self):
        return "FN"
