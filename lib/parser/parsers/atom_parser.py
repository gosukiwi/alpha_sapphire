from lib.parser.parsers.base_parser import BaseParser

# Atoms are entities which can receive messages.
# Atoms are constructs which can be inside a binop or unop.
#
# atom := "(" <expression> ")" # NOTE: maybe this should be "(" <atom> ")" ?
#       | <call_chain>
#       | <integer>
#       | <string>
# TODO:
#       | <hash_access>
#       | <array_access>
#
class AtomParser(BaseParser):
    def is_match(self, tokens):
        return self._try_parsers([
            'identifier',
            'string',
            'integer'
        ], tokens)
