from lib.parser.parsers.parser_factory import ParserFactory
from lib.exceptions import SyntaxErrorException

class Parser:
    def parse(self, tokens):
        statements = []
        while len(tokens) > 0:
            parser = ParserFactory.parser_for('expression')
            if parser.is_match(tokens):
                if parser.consumed == 0:
                    raise Exception("The expression matched but no tokens were consumed.")

                statements.append(parser.node)
                tokens = tokens[parser.consumed:]
            else:
                raise SyntaxErrorException("Invalid statement")
                #raise SyntaxErrorException("Invalid statement" % ' '.join(t.__str__() for t in tokens))
        return statements
