from lib.interpreter.evaluators.evaluator import Evaluator

class AssignEvaluator(Evaluator):
    def eval(self, node, context):
        name = self.eval_one(node.target, context)
        value = self.eval_one(node.value, context)
        context[name] = value
