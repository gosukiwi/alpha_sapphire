from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.call_node import CallNode


# call := <atom> <parg_list>? BOPEN <expression> BCLOSE # TODO
#       | <atom> <parg_list>?
#
# NOTE: Because a simple call is also a valid atom, the check must be
# performed at run-time by the interpreter. The interpreter should try match a
# call, and only try with an atom if the call failed.
#
class CallParser(BaseParser):
    def is_match(self, tokens):
        atom_parser = self._parser_for('atom')
        if not atom_parser.is_match(tokens):
            return False
        consumed = atom_parser.consumed

        pargs_parser = self._parser_for('parg_list')
        pargs_parser.is_match(tokens[consumed:]) # We don't care if it doesn't match, as it's optional
        consumed += pargs_parser.consumed        # Just bump the consumed in case it matched

        self.node = CallNode(message=atom_parser.node, args=pargs_parser.node)
        self.consumed = consumed
        return True
