from lib.interpreter.objects.object import Object

# Native class representing an integer in alpha_sapphire
class Symbol(Object):
    def __init__(self, value=''):
        Object.__init__(self)
        self.value = value
        self.type = 'Symbol'

    def to_string(self):
        return self.value
