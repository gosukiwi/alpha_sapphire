from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.integer_node import IntegerNode

class IntegerParser(BaseParser):
    def is_match(self, tokens):
        if not self._peek(tokens, "INTEGER"):
            return False

        self.node = IntegerNode(int(tokens[0].value))
        self.consumed = 1
        return True
