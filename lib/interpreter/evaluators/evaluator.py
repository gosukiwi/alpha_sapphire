import lib.interpreter.evaluators.factory

# Base class with utility methods for evaluators
class Evaluator:
    def eval_one(self, node, context):
        visitor = self._eval_for(node.name)
        return visitor.eval(node, context)

    def eval_many(self, nodes, context):
        result = []

        if len(nodes) > 0:
            for node in nodes:
                result.append(self.eval_one(node, context))

        return result

    def _eval_for(self, name):
        return lib.interpreter.evaluators.factory.Factory.eval_for(name)
