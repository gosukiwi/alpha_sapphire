from lib.tokenizer.tokens.string_token import StringToken
from lib.tokenizer.matchers.matcher import Matcher

class StringMatcher(Matcher):
    def is_match(self, text):
        quote = u''
        if text[0] != '"' and text[0] != "'":
            return False
        else:
            quote = text[0]

        self._len = 1
        limit     = len(text)
        result    = u""
        while self._len < limit:
            char = text[self._len]
            self._len += 1 # let's now point to the next character

            if char == "\\" and text[self._len] == quote:
                result += text[self._len]
                self._len += 1
            elif char == quote:
                break
            else:
                result += char

        if self._len == 1 or text[self._len - 1] != quote:
            return False

        self.token = StringToken(result, self.quote_type_for(quote))
        return True

    def quote_type_for(self, quote):
        if quote == "'":
            return "SINGLE"
        else:
            return "DOUBLE"
