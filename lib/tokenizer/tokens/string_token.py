from lib.tokenizer.tokens.token import Token

class StringToken(Token):
    def __init__(self, value, type, line=0, col=0):
        Token.__init__(self, "STRING", value, line, col)
        self.type = type

    def __str__(self):
        return "[%s `%s' %s]" % (self.name, self.value, self.type)
