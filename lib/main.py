from lib.tokenizer.tokenizer import Tokenizer
from lib.parser.parser import Parser
from lib.interpreter.interpreter import Interpreter

# Entry point for aplha_sapphire.
def main(debug=True):
    src = u"""
    a = "hi"
    a
    """
    # TODO: Test with this --v
    # a = Integer.new()
    # TODO: Test with this --^

    tokenizer = Tokenizer()
    parser = Parser()
    interpreter = Interpreter(debug=debug)

    interpreter.eval(parser.parse(tokenizer.tokenize(src)))
