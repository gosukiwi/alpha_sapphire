from lib.interpreter.objects.object import Object

# Native class representing an integer in alpha_sapphire
class Integer(Object):
    def __init__(self, value=0):
        Object.__init__(self)
        self.value = value
        self.type = 'Integer'

    def to_string(self):
        return str(self.value)
