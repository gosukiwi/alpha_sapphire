from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.binop_node import BinopNode
from lib.parser.nodes.node import Node

# binop := <smaller_binop> <op> <same_binop>
#        | <call_chain>
# %operation_precedence% (things which are computed first) => Multiplication, Division, Addition, Substraction
#
# NOTE: Precedence is specified in reverse order (from the most generic to the
# more specific). Like splitting terms in an atirhmetic exercise.
#
# a + b * c => a + (b * c)    In this case, the most "generic" is the addition
#|_| |____|                   operation
#
class BinopParser(BaseParser):
    def __init__(self):
        BaseParser.__init__(self)
        self._precedence = {
            'MINUS': 'PLUS',
            'PLUS': 'DIV',
            'DIV': 'TIMES',
            'TIMES': 'PEXPR'
        }
        self.op = 'MINUS' # Initial

    def _parser_for_lhs(self):
        next_op = self._precedence[self.op]
        if next_op == 'PEXPR':
            return self._parser_for('pexpr_or_chain') # Parenthesized Expression Or Chain Call
        else:
            parser = BinopParser()
            parser.op = next_op
            return parser

    def is_match(self, tokens):
        # Match LHS
        lhs = Node.none
        consumed = 0
        parser = self._parser_for_lhs()
        if parser.is_match(tokens):
            lhs = parser.node
            consumed = parser.consumed
        else:
            return False

        # Try match operation and RHS
        if consumed < len(tokens) and self.op == tokens[consumed].name:
            consumed += 1 # consume operation token
            binop_parser = BinopParser()
            binop_parser.op = self.op
            if not binop_parser.is_match(tokens[consumed:]):
                return False

            consumed += binop_parser.consumed
            rhs = binop_parser.node
            self.node = BinopNode(lhs=lhs, op=self.op, rhs=rhs)
            self.consumed = consumed
            return True

        # Otherwise just return the LHS on it's own
        self.node = lhs
        self.consumed = consumed
        return True
