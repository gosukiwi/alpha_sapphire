from lib.interpreter.evaluators.assign_evaluator import AssignEvaluator
from lib.interpreter.evaluators.integer_evaluator import IntegerEvaluator
from lib.interpreter.evaluators.string_evaluator import StringEvaluator
from lib.interpreter.evaluators.identifier_evaluator import IdentifierEvaluator
from lib.interpreter.evaluators.call_evaluator import CallEvaluator

class Factory:
    evaluators = {
        'ASSIGN': AssignEvaluator(),
        'INTEGER': IntegerEvaluator(),
        'STRING': StringEvaluator(),
        'IDENTIFIER': IdentifierEvaluator(),
        'CALL': CallEvaluator()
    }

    @staticmethod
    def eval_for(name):
        return Factory.evaluators[name]
