from lib.parser.nodes.node import Node

class IdentifierNode(Node):
    def __init__(self, identifier):
        Node.__init__(self, "IDENTIFIER", None)
        self.identifier = identifier

    def __str__(self):
        return self.identifier
