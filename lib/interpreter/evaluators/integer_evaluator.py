from lib.interpreter.evaluators.evaluator import Evaluator
from lib.interpreter.objects.integer import Integer

class IntegerEvaluator(Evaluator):
    def eval(self, node, context):
        return Integer(value=node.integer)
