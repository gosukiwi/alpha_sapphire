from lib.tokenizer.matchers.matcher import Matcher

class Whitespace(Matcher):
    def is_match(self, text):
        self._len = 0
        limit     = len(text)
        while self._len < limit:
            char = text[self._len]
            if char == ' ' or char == "\t" or char == "\n":
                self._len += 1
            else:
                break
        return self._len > 0
