from lib.tokenizer.matchers.whitespace import Whitespace
from lib.tokenizer.matchers.simple import Simple
from lib.tokenizer.matchers.identifier import Identifier
from lib.tokenizer.matchers.string_matcher import StringMatcher
from lib.tokenizer.matchers.integer_matcher import IntegerMatcher
from lib.exceptions import SyntaxErrorException

class Tokenizer:
    def __init__(self):
        self._tokens = []
        self._set_up_matchers()

    def _set_up_matchers(self):
        self._matchers = [
            Whitespace(),
            Simple('FN', u'fn'),
            Simple('END', u'end'),
            Simple('ARROW', u'->'),
            Simple('EQUALS', u'='),
            Simple('POPEN', u'('),
            Simple('PCLOSE', u')'),
            Simple('PLUS', u'+'),
            Simple('MINUS', u'-'),
            Simple('DIV', u'/'),
            Simple('TIMES', u'*'),
            Simple('COMMA', u','),
            Simple('DOT', u'.'),
            #SimpleMulti('TRUE', [u'true', u'yes', u'on']),
            #NumberMatcher(),
            StringMatcher(),
            IntegerMatcher(),
            Identifier(),
        ]

    def tokenize(self, text):
        self._text = text
        while len(self._text) > 0:
            for matcher in self._matchers:
                if self.try_matcher(matcher):
                    break
            else:
                raise SyntaxErrorException(u"TOKENIZER: Could not match `%s'" % self._text)

        return self._tokens

    def try_matcher(self, matcher):
        if not matcher.is_match(self._text):
            return False

        self._text = matcher.consume(self._text)
        if matcher.token: # if it's not None
            self._tokens.append(matcher.token)
        return True
