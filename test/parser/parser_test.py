from lib.parser.parser import Parser
from lib.tokenizer.tokenizer import Tokenizer

def parse(text):
    tokenizer = Tokenizer()
    parser = Parser()
    tokens = tokenizer.tokenize(text)
    return parser.parse(tokens)

def parse_one(text):
    return parse(text)[0]

def test_matches_assign():
    statement = parse_one("foo = 'hello'")
    assert statement.name == 'ASSIGN'
    assert statement.target.identifier == 'foo'
    assert statement.value.name == 'CALL'
    assert statement.value.message.name == 'STRING'
    assert statement.value.message.content == 'hello'

    statement = parse_one('foo = ("hello")')
    assert statement.name == 'ASSIGN'
    assert statement.target.name == 'IDENTIFIER'

    statement = parse_one('foo = 2 + 2')
    assert statement.name == 'ASSIGN'

    statement = parse_one('foo = bar')
    assert statement.name == 'ASSIGN'

    statement = parse_one('foo = bar()')
    assert statement.name == 'ASSIGN'

    statement = parse_one('foo = bar.baz')
    assert statement.name == 'ASSIGN'

    statement = parse_one('foo = 1.bar.baz')
    assert statement.name == 'ASSIGN'

def test_matches_binop():
    statement = parse_one('"foo" + "bar"')
    assert statement.name == 'BINOP'
    assert statement.op == 'PLUS'
    assert statement.lhs.name == 'CALL'
    assert statement.lhs.message.name == 'STRING'
    assert statement.lhs.message.content == 'foo'
    assert statement.rhs.name == 'CALL'

    statement = parse_one('2 + (bar * "baz")')
    lhs = statement.lhs
    rhs = statement.rhs
    assert statement.op == 'PLUS'
    assert lhs.name == 'CALL'
    assert lhs.message.name == 'INTEGER'
    assert lhs.message.integer == 2
    assert rhs.op == 'TIMES'
    assert rhs.lhs.name == 'CALL'
    assert rhs.lhs.message.identifier == 'bar'

def test_precedence():
    statement = parse_one('a * b / c - a + b')
    assert statement.op == 'MINUS'
    assert statement.lhs.op == 'DIV'
    assert statement.rhs.op == 'PLUS'

def test_matches_integers():
    statement = parse_one("foo = 11")
    assert statement.target.identifier == 'foo'
    assert statement.value.name == 'CALL'
    assert statement.value.message.name == 'INTEGER'
    assert statement.value.message.integer == 11

def test_matches_fn():
    statement = parse_one("fn foo -> 2 end")
    assert statement.name == 'FN'
    assert statement.args.nodes[0].identifier == 'foo'

def test_matches_call():
    statement = parse_one("foo(1)")
    assert statement.name == 'CALL'

    statement = parse_one("foo(1).bar.baz")
    assert statement.name == 'CALL'
    assert statement.chain.chain.message.identifier == 'baz'
