from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.identifier_node import IdentifierNode

# identifier := IDENTIFIER
# TODO
#             | IDENTIFIER BOPEN <expression> BCLOSE
class IdentifierParser(BaseParser):
    def is_match(self, tokens):
        if not self._peek(tokens, "IDENTIFIER"):
            return False

        self.node = IdentifierNode(tokens[0].value)
        self.consumed = 1
        return True
