from lib.tokenizer.matchers.simple import Simple
import pytest

@pytest.fixture
def matcher():
    return Simple("FOO", 'foo')

def test_matches_foo(matcher):
    assert matcher.is_match('foo') == True

def test_matches_foo(matcher):
    assert matcher.is_match('not_foo') == False

def test_consumes_foo(matcher):
    assert matcher.consume('foo bar') == ' bar'

def test_return_token(matcher):
    matcher.is_match('foo')
    assert matcher.token.name == 'FOO'
    assert matcher.token.value == 'foo'
