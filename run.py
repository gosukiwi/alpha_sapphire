from lib.main import main

# Boilerplate, just run `main`
def entry_point(argv):
    main(debug=False)
    return 0

def target(*args):
    return entry_point

if __name__ == "__main__":
    main()
