from lib.parser.parsers.base_parser import BaseParser
from lib.parser.nodes.node import Node

# Unary Operation:
#
#  unop := NOT <expression>
#
# Note that they are specified in the inverse order.
class UnopParser(BaseParser):
    def is_match(self, tokens):
        if not self._peek(tokens, 'NOT'):
            return False

        consumed = 1
        parser = self._parser_for('expression')
        if not parser.is_match(tokens[consumed:]):
            return False
        consumed += parser.consumed

        self.node = Node(name='UNOP', value=parser.node)
        self.consumed = consumed
        return True
