from lib.parser.parsers.parser_factory import ParserFactory
from lib.tokenizer.tokenizer import Tokenizer

def tokenize(text):
    return Tokenizer().tokenize(text)

def test_one():
    parser = ParserFactory.parser_for('expression_list')
    assert parser.is_match(tokenize("""
        a = 1
        a + b
    end
    """)) == True
    assert len(parser.node.nodes) == 2
    assert parser.node.nodes[0].name == 'ASSIGN'
    assert parser.node.nodes[1].name == 'BINOP'
