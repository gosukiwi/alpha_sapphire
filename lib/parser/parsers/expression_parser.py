from lib.parser.parsers.base_parser import BaseParser

# expression := <assign>
#             | <function_definition>
#             | <call>
#             | <unaryop>
#             | <binop>
#
class ExpressionParser(BaseParser):
    def is_match(self, tokens):
        return self._try_parsers([
            'assign',
            'fn',
            'unop',
            'binop'
        ], tokens)
